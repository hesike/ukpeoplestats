UkPeopleStats
=================

Tool for processing UK data downloaded from https://www.briandunning.com/sample-data/

Visual Studio Community 2019

WPF, .NET Core 3.1

Entity Framework Core 5.0.10 for SQLite

![AppImg](Other/screen.png)

**Functionality:**

Importing data from CSV to SQLite DB (this is slow, because it goes through EF)

Showing loaded data (people) with some limited filtering

Adding new person, editing existing person (without contacts)

Statistics:
* Most frequented first names
* Counties with most people
* Most used email domains
* Most used web domains
* Locations with largest groups of people

Statistics are computed and the start of the program and needs to be reloaded manually after the data are changed (this is to prevent continuous reloading when user needs to make many edits)