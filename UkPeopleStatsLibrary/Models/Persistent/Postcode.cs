﻿using System;
using System.ComponentModel;

namespace UkPeopleStatsLibrary.Models.Persistent
{
    public class Postcode : IdModelBase, IDataErrorInfo
    {
        private string _code;

        // non processed data have both null, not found data have only latitude filled, see IsProcessed and IsInvalid properties
        private double? _latitude;
        private double? _longitude;



        public Postcode()
        {
        }

        public string Code
        {
            get { return _code; }
            set
            {
                _code = value;
                OnPropertyChanged();
            }
        }

        public double? Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged();
            }
        }

        public double? Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged();
            }
        }

        public bool IsProcessed
        {
            get { return _latitude != null; }
        }

        public bool IsInvalid
        {
            get { return _latitude != null && _longitude == null; }
        }

        public string Error => "";

        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                if (columnName == "Code")
                {
                    const int CodeMaxLength = 10;
                    if (String.IsNullOrWhiteSpace(Code))
                    {
                        return "Postcode must be filled.";
                    }
                    else if (Code.Length > CodeMaxLength)
                    {
                        return "Code can't be longer than " + CodeMaxLength;
                    }
                }

                return null;
            }
        }

        public override void ReplaceDirectData(object other, bool changeIdentity)
        {
            Postcode otherPostcode = other as Postcode;

            if (otherPostcode != null)
            {
                if (changeIdentity)
                {
                    Id = otherPostcode.Id;
                }

                Code = otherPostcode.Code;

                Latitude = otherPostcode.Latitude;
                Longitude = otherPostcode.Longitude;
            }
        }

        public void MarkAsInvalid()
        {
            Latitude = 0;
            Longitude = null;
        }
    }
}
