﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models.Persistent
{

    public class Contact : IdModelBase
    {
        public enum ContactType
        {
            Phone,
            Email,
            Web
        }

        private ContactType _type;

        private string _value;

        public ContactType Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged();
            }
        }

        public string Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged();
            }
        }

        public override void ReplaceDirectData(object other, bool changeIdentity)
        {
            Contact otherContact = other as Contact;

            if (otherContact != null)
            {
                if (changeIdentity)
                {
                    Id = otherContact.Id;
                }

                Type = otherContact.Type;
                Value = otherContact.Value;
            }
        }
    }
}
