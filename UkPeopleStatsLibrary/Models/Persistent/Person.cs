﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace UkPeopleStatsLibrary.Models.Persistent
{
    public class Person : IdModelBase, IDataErrorInfo
    {
        private string _firstName;
        private string _lastName;
        private string _company;
        private string _address;
        private string _city;
        private string _county;
        private Postcode _postcode;
        private ICollection<Contact> _contacts = new ObservableCollection<Contact>();

        public Person()
        {
        }



        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged();
            }
        }

        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }

        public string Company
        {
            get { return _company; }
            set
            {
                _company = value;
                OnPropertyChanged();
            }
        }

        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                OnPropertyChanged();
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                _city = value;
                OnPropertyChanged();
            }
        }

        public string County
        {
            get { return _county; }
            set
            {
                _county = value;
                OnPropertyChanged();
            }
        }

        public Postcode Postcode
        {
            get { return _postcode; }
            set
            {
                _postcode = value;
                OnPropertyChanged();
            }
        }

        public ICollection<Contact> Contacts
        {
            get { return _contacts; }
        }

        
        #region Validations
        public string Error => "";
        
        string IDataErrorInfo.this[string columnName]
        {
            get
            {
                const int MaxStringLength = 100;

                if (columnName == "FirstName")
                {
                    return ValidateString("First name", FirstName, MaxStringLength);
                }
                else if (columnName == "LastName")
                {
                    return ValidateString("Last name", LastName, MaxStringLength);
                }
                else if (columnName == "Company")
                {
                    return ValidateString("Company", Company, MaxStringLength);
                } 
                else if (columnName == "Address")
                {
                    return ValidateString("Address", Address, MaxStringLength);
                }
                else if (columnName == "City")
                {
                    return ValidateString("City", City, MaxStringLength);
                }
                else if (columnName == "County")
                {
                    return ValidateString("County", County, MaxStringLength);
                }
                else if (columnName == "Postcode")
                {
                    return ValidateString("Postcode", Postcode.Code, MaxStringLength);
                }

                return null;
            }
        }

        private string ValidateString(string name, string value, int maxLength)
        {
            if (String.IsNullOrWhiteSpace(value))
            {
                return name + " must be filled.";
            }
            else if (value.Length > maxLength)
            {
                return name + " can't be longer than " + maxLength;
            }

            return null;
        }

        static readonly string[] ValidatedProperties =
        {
            "FirstName",
            "LastName",
            "Company",
            "Address",
            "City",
            "County",
            "Postcode"
        };

        public bool IsValid
        {
            get
            {
                foreach (string property in ValidatedProperties)
                {

                    if (((IDataErrorInfo)this)[property] != null) // there is an error
                        return false;
                }

                return true;
            }
        }
        #endregion
        

        public override void ReplaceDirectData(object other, bool changeIdentity)
        {
            Person otherPerson = other as Person;

            if (otherPerson != null)
            {
                if (changeIdentity)
                {
                    Id = otherPerson.Id;
                }

                FirstName = otherPerson.FirstName;
                LastName = otherPerson.LastName;
                Company = otherPerson.Company;
                Address = otherPerson.Address;
                City = otherPerson.City;
                County = otherPerson.County;

                
                if (otherPerson.Postcode == null)
                {
                    Postcode = null;
                }
                else if (changeIdentity)
                {
                    // fill the data only for transition db -> ui, not the other
                    Postcode = new Postcode();
                    Postcode.ReplaceDirectData(otherPerson.Postcode, true);
                }

            }
        }
    }
}
