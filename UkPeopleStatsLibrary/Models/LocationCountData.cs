﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models
{
    public class LocationCountData
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public int Count { get; set; }

    }
}
