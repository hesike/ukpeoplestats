﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models.ProgressReports
{
    public class FinishedProgressReport
    {
        public bool IsFinished { get; set; }
        public bool IsError { get; set; }

        public string ErrorMessage { get; set; }
    }
}
