﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models.ProgressReports
{
    public class PopularNamesProgressReport : PercentageProgressReport
    {
        public List<NameCount> PopularNames { get; set; }
    }
}
