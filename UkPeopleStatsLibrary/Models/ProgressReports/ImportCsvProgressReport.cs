﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models.ProgressReports
{
    public class ImportCsvProgressReport : FinishedProgressReport
    {
        public int TotalImported { get; set; }
    }
}
