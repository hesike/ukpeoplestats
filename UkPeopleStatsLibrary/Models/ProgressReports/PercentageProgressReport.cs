﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models.ProgressReports
{
    public class PercentageProgressReport : FinishedProgressReport
    {
        public int MaxPercent { get; set; } = 100;

        public int Percent { get; set; }
    }
}
