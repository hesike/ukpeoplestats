﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models.ProgressReports
{
    public class ClusterDataProgressReport : PercentageProgressReport
    {
        public List<LocationCountData> TopClusters { get; set; }
    }
}
