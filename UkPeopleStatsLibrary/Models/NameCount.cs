﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace UkPeopleStatsLibrary.Models
{
    public class NameCount : IComparable<NameCount>
    {
        public string Name { get;  set; }
        public int Count { get; set; }

        public NameCount() { }

        public NameCount(string domain, int count)
        {
            Name = domain;
            Count = count;
        }

        public int CompareTo([AllowNull] NameCount other)
        {
            if (other == null)
            {
                return 1;
            }

            if (Count != other.Count)
            {
                return Count.CompareTo(other.Count);
            }

            return Name.CompareTo(other.Name);
        }
    }
}
