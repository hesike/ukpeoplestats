﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models
{
    public abstract class IdModelBase : NotifyPropertyChangedBase, IDataReplaceable
    {
        private int? _id;

        public int? Id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        // since we're using EF and "SaveChanges", we don't want to let UI bind on (and modify) the DB data, so we're making copies and this makes it easier to transport data back and forth
        public abstract void ReplaceDirectData(object other, bool changeIdentity);
    }
}
