﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.Models
{
    public static class UkConstants
    {
        // UK data - min and max latitudes/longitudes - used for sorting people in a grid to find biggest groups of people close to each other
        public const int MinLat = 49;
        public const int MaxLat = 61;

        public const int MinLon = -9;
        public const int MaxLon = 2;

    }
}
