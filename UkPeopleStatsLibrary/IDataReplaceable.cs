﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary
{
    public interface IDataReplaceable
    {
        public void ReplaceDirectData(object other, bool changeIdentity);
    }
}
