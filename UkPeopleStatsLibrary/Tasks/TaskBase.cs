﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace UkPeopleStatsLibrary.Tasks
{
    public abstract class TaskBase<T>
    {
        protected IProgress<T> Progress { get; }
        protected CancellationToken CancelToken { get; }

        public TaskBase(IProgress<T> progress, CancellationToken cancelToken)
        {
            Progress = progress;
            CancelToken = cancelToken;
        }

        public abstract Task RunAsync();
    }
}
