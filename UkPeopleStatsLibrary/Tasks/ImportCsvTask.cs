﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models.Persistent;
using UkPeopleStatsLibrary.Models.ProgressReports;

namespace UkPeopleStatsLibrary.Tasks
{
    public class ImportCsvTask : TaskBase<ImportCsvProgressReport>
    {
        private AppDbContext _dbContext;

        private string _filePath;

        private const int _saveLimit = 50;

        private enum CsvColumns
        {
            FirstName = 0,
            LastName,
            Company,
            Address,
            City,
            County,
            Postal,
            PhoneFirst,
            PhoneSecond,
            Email,
            Web
        }

        public ImportCsvTask(string filePath, IProgress<ImportCsvProgressReport> progress, CancellationToken cancelToken) : base(progress, cancelToken)
        {
            _dbContext = new AppDbContext();
            _filePath = filePath;
        }

        public async override Task RunAsync()
        {
            await Task.Run(() =>
            {
                ImportCsvProgressReport progressReport = new ImportCsvProgressReport();

                int totalImported = 0;
                int unsaved = 0;

                try
                {
                    using (TextFieldParser parser = new TextFieldParser(_filePath))
                    {
                        parser.TextFieldType = FieldType.Delimited;
                        parser.SetDelimiters(",");
                        // not used, for now just for ignoring first row
                        string[] columns = parser.ReadFields();
                        while (!parser.EndOfData)
                        {
                            string[] fields = parser.ReadFields();

                            if (fields.Length >= Enum.GetNames(typeof(CsvColumns)).Length)
                            {

                                AddPerson(fields[(int)CsvColumns.FirstName],
                                    fields[(int)CsvColumns.LastName],
                                    fields[(int)CsvColumns.Company],
                                    fields[(int)CsvColumns.Address],
                                    fields[(int)CsvColumns.City],
                                    fields[(int)CsvColumns.County],
                                    fields[(int)CsvColumns.Postal],
                                    fields[(int)CsvColumns.PhoneFirst],
                                    fields[(int)CsvColumns.PhoneSecond],
                                    fields[(int)CsvColumns.Email],
                                    fields[(int)CsvColumns.Web]);

                                totalImported++;
                            }


                            unsaved++;

                            if (unsaved > _saveLimit)
                            {
                                Save(ref unsaved, totalImported, progressReport);
                            }

                            CancelToken.ThrowIfCancellationRequested();

                        }
                    }



                    progressReport.IsFinished = true;
                    progressReport.IsError = false;
                    Save(ref unsaved, totalImported, progressReport);

                }
                catch (Exception e)
                {
                    progressReport.IsFinished = true;
                    progressReport.IsError = true;
                    progressReport.ErrorMessage = e.Message;
                    Save(ref unsaved, totalImported, progressReport);
                    return;
                }
            });
        }

        private void Save(ref int unsaved, int totalImported, ImportCsvProgressReport progressReport)
        {
            _dbContext.SaveChanges();
            unsaved = 0;

            progressReport.TotalImported = totalImported;
            Progress.Report(progressReport);
        }

        private Contact CreateAndSaveContact(Contact.ContactType type, string value)
        {
            Contact contact = new Contact();
            contact.Type = type;
            contact.Value = value;
            _dbContext.Contacts.Add(contact);

            return contact;
        }

        private void AddPerson(string firstNameInput, string lastNameInput, string companyInput, string addressInput, string cityInput, string countyInput, string postalInput, string phone1Input, string phone2Input, string emailInput, string webInput)
        {
            bool immediateSaveRequired = false;
            Person person = new Person();

            person.FirstName = firstNameInput;
            person.LastName = lastNameInput;
            person.Company = companyInput;
            person.Address = addressInput;
            person.City = cityInput;
            person.County = countyInput;

            // handle postcode
            person.Postcode = _dbContext.Postcodes.Where(p => p.Code == postalInput).FirstOrDefault();
        

            if (person.Postcode == null)
            {
                person.Postcode = new Postcode();
                person.Postcode.Code = postalInput;
                _dbContext.Postcodes.Add(person.Postcode);

                // save immediatelly because the next data may need to use it (this makes the whole process really slow)
                immediateSaveRequired = true;
            }

            // handle contacts
            person.Contacts.Add(CreateAndSaveContact(Contact.ContactType.Phone, phone1Input));
            person.Contacts.Add(CreateAndSaveContact(Contact.ContactType.Phone, phone2Input));
            person.Contacts.Add(CreateAndSaveContact(Contact.ContactType.Email, emailInput));
            person.Contacts.Add(CreateAndSaveContact(Contact.ContactType.Web, webInput));

            // TODO RK improve - check if person is valid

            _dbContext.People.Add(person);

            if (immediateSaveRequired)
            {
                _dbContext.SaveChanges();
            }

        }
    }
}
