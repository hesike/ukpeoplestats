﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models.ProgressReports;
using System.Linq;
using UkPeopleStatsLibrary.Models;

namespace UkPeopleStatsLibrary.Tasks
{

    public class CalculatePopularFirstNamesTask : TaskBase<PopularNamesProgressReport>
    {

        private int _topCount;

        public CalculatePopularFirstNamesTask(int topCount, IProgress<PopularNamesProgressReport> progress, CancellationToken cancelToken) : base(progress, cancelToken)
        {
            _topCount = topCount;
        }

        public async override Task RunAsync()
        {
            await Task.Run(() => {

                PopularNamesProgressReport report = new PopularNamesProgressReport();

                try
                {
                    AppDbContext dbContext = new AppDbContext();

                    // load data from db (no progress tracked)
                    report.PopularNames = new List<NameCount>(dbContext.People.GroupBy(p => p.FirstName).Select(g => new NameCount { Name = g.Key, Count = g.Count() }).OrderByDescending(nc => nc.Count).ThenBy(nc => nc.Name).Take(_topCount));
                    report.IsFinished = true;
                    report.Percent = report.MaxPercent;
                    Progress.Report(report);
                }
                catch (Exception e)
                {
                    // something went wrong, report the error
                    report.IsFinished = true;
                    report.IsError = true;
                    report.ErrorMessage = e.Message;
                    Progress.Report(report);
                }

            });
        }
    }
}
