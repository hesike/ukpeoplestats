﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models;
using UkPeopleStatsLibrary.Models.ProgressReports;

namespace UkPeopleStatsLibrary.Tasks
{
    class CalculatePopularCountiesTask : TaskBase<PopularNamesProgressReport>
    {

        private int _topCount;

        public CalculatePopularCountiesTask(int topCount, IProgress<PopularNamesProgressReport> progress, CancellationToken cancelToken) : base(progress, cancelToken)
        {
            _topCount = topCount;
        }

        public async override Task RunAsync()
        {
            await Task.Run(() => {

                PopularNamesProgressReport report = new PopularNamesProgressReport();

                try
                {
                    AppDbContext dbContext = new AppDbContext();

                    // load data from db (no progress tracked)
                    report.PopularNames = new List<NameCount>(dbContext.People.GroupBy(p => p.County).Select(g => new NameCount { Name = g.Key, Count = g.Count() }).OrderByDescending(nc => nc.Count).ThenBy(nc => nc.Name).Take(_topCount));
                    report.IsFinished = true;
                    report.Percent = report.MaxPercent;
                    Progress.Report(report);
                }
                catch (Exception e)
                {
                    // something went wrong, report the error
                    report.IsFinished = true;
                    report.IsError = true;
                    report.ErrorMessage = e.Message;
                    Progress.Report(report);
                }

            });
        }
    }
}
