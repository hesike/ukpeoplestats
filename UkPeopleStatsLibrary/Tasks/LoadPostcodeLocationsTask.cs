﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models.Persistent;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Services;

namespace UkPeopleStatsLibrary.Tasks
{
    public class LoadPostcodeLocationsTask : TaskBase<PercentageProgressReport>
    {
        private PostcodeService _postcodeService = new PostcodeService();

        private const int _batchSize = 10;


        public LoadPostcodeLocationsTask(IProgress<PercentageProgressReport> progress, CancellationToken cancelToken) : base(progress, cancelToken)
        {

        }

        public async override Task RunAsync()
        {
            await Task.Run(async () =>
            {
                PercentageProgressReport report = new PercentageProgressReport();

                try
                {
                    AppDbContext dbContext = new AppDbContext();

                    int totalData = dbContext.Postcodes.Count();

                    int processedData = 0;
                    int unsaved = 0;

                    List<Postcode> postcodes = dbContext.Postcodes.Where(p => p.Latitude == null).ToList();

                    totalData = postcodes.Count;

                    foreach (Postcode postcode in postcodes)
                    {
                        // TODO RK improve - load in parallel

                        // fill data for the postcode
                        await _postcodeService.FillLocationAsync(postcode);

                        processedData++;
                        unsaved++;

                        if (unsaved >= _batchSize)
                        {
                            // save the changes
                            dbContext.SaveChanges();
                            unsaved = 0;

                            report.Percent = processedData * report.MaxPercent / totalData;
                            Progress.Report(report);

                            CancelToken.ThrowIfCancellationRequested();
                        }

                    }

                    // save rest of the changes
                    dbContext.SaveChanges();

                    // report finishing the task
                    report.IsFinished = true;
                    report.Percent = report.MaxPercent;
                    Progress.Report(report);
                }
                catch (Exception e)
                {
                    // something went wrong, report the error
                    report.IsFinished = true;
                    report.IsError = true;
                    report.ErrorMessage = e.Message;
                    Progress.Report(report);
                }

            });
        }

    }
}
