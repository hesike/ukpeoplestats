﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models.ProgressReports;
using System.Linq;
using UkPeopleStatsLibrary.Models;
using static UkPeopleStatsLibrary.Models.Persistent.Contact;

namespace UkPeopleStatsLibrary.Tasks
{

    public class CalculatePopularDomainsTask : TaskBase<PopularNamesProgressReport>
    {

        private Func<string, string> _splitDomainFunc;
        private ContactType _contactType;

        private int _topCount;

        public CalculatePopularDomainsTask(int topCount, ContactType contactType, Func<string, string> splitDomainFunc, IProgress<PopularNamesProgressReport> progress, CancellationToken cancelToken) : base(progress, cancelToken)
        {
            _topCount = topCount;
            _contactType = contactType;
            _splitDomainFunc = splitDomainFunc;
        }

        public async override Task RunAsync()
        {
            // note: this would be a lot easier in the database, especially if we had the domain as separate column and then used group by, but I'm not sure about splitting the data just for this one small feature


            await Task.Run(() => {

                PopularNamesProgressReport report = new PopularNamesProgressReport();

                try
                {
                    AppDbContext dbContext = new AppDbContext();

                    List<string> allData = dbContext.Contacts.Where(c => c.Type == _contactType).Select(c => c.Value).ToList();

                    Dictionary<string, int> domainFrequency = new Dictionary<string, int>();

                    int totalWork = allData.Count;

                    // just some treshold for reporting the data, doesn't matter much
                    int reportTreshold = 10 + allData.Count / 50;

                    int processed = 0;
                    int unreported = 0;

                    // first part: group domains
                    foreach (string contact in allData)
                    {
                        // get the domain (using the provided function)
                        string domain = _splitDomainFunc(contact);

                        if (domain != null)
                        {
                            int count = domainFrequency.GetValueOrDefault(domain) + 1;
                            domainFrequency[domain] = count;
                        }

                        processed++;
                        unreported++;

                        if (unreported >= reportTreshold)
                        {
                            unreported = 0;
                            // report up to 50% of work (there's still second part of the task)
                            report.Percent = processed * (report.MaxPercent / 2) / totalWork;
                            Progress.Report(report);

                            CancelToken.ThrowIfCancellationRequested();
                        }

                    }

                    // second part: find largest groups
                    totalWork += domainFrequency.Count;

                    SortedSet<NameCount> popularEmails = new SortedSet<NameCount>();


                    foreach (var entry in domainFrequency)
                    {
                        popularEmails.Add(new NameCount(entry.Key, entry.Value));

                        if (popularEmails.Count > _topCount)
                        {
                            popularEmails.Remove(popularEmails.Min);
                        }

                        processed++;
                        unreported++;

                        if (unreported >= reportTreshold)
                        {
                            unreported = 0;
                            // report up to 100% of work
                            report.Percent = processed * report.MaxPercent / totalWork;
                            Progress.Report(report);

                            CancelToken.ThrowIfCancellationRequested();
                        }
                    }

                    // we are finished, fill the data
                    report.PopularNames = new List<NameCount>(popularEmails.Reverse());
                    report.IsFinished = true;
                    report.Percent = report.MaxPercent;
                    Progress.Report(report);
                }
                catch (Exception e)
                {
                    // something went wrong, report the error
                    report.IsFinished = true;
                    report.IsError = true;
                    report.ErrorMessage = e.Message;
                    Progress.Report(report);
                }

            });
        }
    }
}
