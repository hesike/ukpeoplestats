﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models.ProgressReports;
using System.Linq;
using UkPeopleStatsLibrary.Models;
using UkPeopleStatsLibrary.Models.Persistent;

namespace UkPeopleStatsLibrary.Tasks
{
    public class CalculatePeopleClustersTask : TaskBase<ClusterDataProgressReport>
    {

        // how many groups we want to find
        const int GroupsToFind = 5;

        // how many cells we want for a square 1x1 (1 latitude difference x 1 longitude difference)
        const int CellsMultiplier = 10;

        const int TotalCellsLat = (UkConstants.MaxLat - UkConstants.MinLat) * CellsMultiplier;
        const int TotalCellsLot = (UkConstants.MaxLon - UkConstants.MinLon) * CellsMultiplier;

        public CalculatePeopleClustersTask(IProgress<ClusterDataProgressReport> progress, CancellationToken cancelToken) : base(progress, cancelToken)
        {

        }

        private class PostcodeUsage
        {
            public int PostcodeId { get; set; }

            public int Count { get; set; }
        }

        private class TopGroupData
        {
            public int LatIndex { get; set; }
            public int LonIndex { get; set; }

            public int Count { get; set; }
        }


        public async override Task RunAsync()
        {
            await Task.Run(() => {

                ClusterDataProgressReport report = new ClusterDataProgressReport();

                try
                {

                    AppDbContext dbContext = new AppDbContext();

                    // TODO RK improve - load this postcode data + usage directly from DB and not as two steps (I've tried but failed to convince Linq to do so)
                    #region Delete this if we can get all data directly from DB
                    Dictionary<int, PostcodeUsage> postcodeUsage = (from person in dbContext.People
                                                                    group person by person.Postcode.Id into personGroup
                                                                    select new PostcodeUsage { PostcodeId = (int)personGroup.Key, Count = personGroup.Count() }).ToDictionary(x => x.PostcodeId);

                    List<Postcode> allPostcodes = (from postcode in dbContext.Postcodes where postcode.Latitude != null && postcode.Longitude != null select postcode).ToList();

                    List<LocationCountData> locationCounts = new List<LocationCountData>();

                    foreach (Postcode postcode in allPostcodes)
                    {
                        if (postcodeUsage.ContainsKey((int)postcode.Id))
                        {
                            int usageCount = postcodeUsage[(int)postcode.Id].Count;
                            if (usageCount > 0)
                            {
                                LocationCountData locationCountData = new LocationCountData();
                                locationCountData.Latitude = (double)postcode.Latitude;
                                locationCountData.Longitude = (double)postcode.Longitude;
                                locationCountData.Count = usageCount;
                                locationCounts.Add(locationCountData);
                            }
                        }
                    }
                    #endregion


                    // FIRST STEP - find areas with most people


                    // 1a - split UK to grid and count people in every cell
                    int[,] GridCounts = new int[TotalCellsLat, TotalCellsLot];

                    int totalData = TotalCellsLat * TotalCellsLot + locationCounts.Count;
                    int processed = 0;
                    int unreported = 0;
                    int reportSize = 1 + totalData / 90;

                    foreach (LocationCountData locationCount in locationCounts)
                    {
                        processed++;
                        unreported++;

                        // find appropriate cell for this location
                        int latIndex = (int)((locationCount.Latitude - UkConstants.MinLat) * CellsMultiplier / (UkConstants.MaxLat - UkConstants.MinLat));
                        int lonIndex = (int)((locationCount.Longitude - UkConstants.MinLon) * CellsMultiplier / (UkConstants.MaxLon - UkConstants.MinLon));

                        if (latIndex < 0 || latIndex >= TotalCellsLat || lonIndex < 0 || lonIndex >= TotalCellsLot)
                        {
                            // this person is somehow not in UK so we're gonna ignore him
                            continue;
                        }

                        // count in all the people for this postcode
                        GridCounts[latIndex, lonIndex] += locationCount.Count;

                        if (unreported >= reportSize)
                        {
                            ReportProgress(ref unreported, processed, totalData, report);
                        }
                    }

                    TopGroupData[] TopGroups = new TopGroupData[GroupsToFind];

                    // "score" of the smallest group still in the top N - this will help us reject groups quickly
                    int MinCountToRank = 0;

                    // 1b - find top N biggest groups
                    for (int lat = 0; lat < TotalCellsLat; lat++)
                    {
                        for (int lon = 0; lon < TotalCellsLot; lon++)
                        {
                            int count = GridCounts[lat, lon];

                            processed++;
                            unreported++;

                            // check if it should be in top groups
                            if (count > 0 && count > MinCountToRank)
                            {
                                TopGroupData processedGroup = new TopGroupData { LatIndex = lat, LonIndex = lon, Count = count };

                                // it should be in top groups, find appropriate place
                                for (int i = 0; i < GroupsToFind; i++)
                                {
                                    TopGroupData group = TopGroups[i];

                                    if (group == null || group.Count < processedGroup.Count)
                                    {
                                        // place the group, sort original group to the rest of the array (basically all values after will move by one)
                                        TopGroups[i] = processedGroup;
                                        processedGroup = group;

                                        if (processedGroup == null)
                                        {
                                            // nothing to continue with
                                            break;
                                        }
                                    }
                                }

                                // update min count to rank (see count at last index)
                                TopGroupData lastGroup = TopGroups[GroupsToFind - 1];
                                if (lastGroup != null)
                                {
                                    MinCountToRank = lastGroup.Count;
                                }
                            }

                            if (unreported >= reportSize)
                            {
                                ReportProgress(ref unreported, processed, totalData, report);
                            }

                        }
                    }

                    List<LocationCountData> topGroupsLocationData = new List<LocationCountData>();
                    // 1c - convert the data to latitudes / longitudes and return results
                    for (int i = 0; i < GroupsToFind; i++)
                    {
                        TopGroupData group = TopGroups[i];

                        if (group != null)
                        {
                            LocationCountData locationCountData = new LocationCountData();
                            // + 0.5 to index is because we want the location to be "in the middle of the cell"
                            locationCountData.Latitude = (group.LatIndex + 0.5) * (UkConstants.MaxLat - UkConstants.MinLat) / CellsMultiplier + UkConstants.MinLat;
                            locationCountData.Longitude = (group.LonIndex + 0.5) * (UkConstants.MaxLon - UkConstants.MinLon) / CellsMultiplier + UkConstants.MinLon;
                            locationCountData.Count = group.Count;

                            topGroupsLocationData.Add(locationCountData);
                        }
                    }

                    report.TopClusters = topGroupsLocationData;
                    report.IsFinished = true;
                    report.Percent = report.MaxPercent;
                    Progress.Report(report);


                    // TODO RK improve - improve the final locations (put them in to the center of the group, to average location)
                    // SECOND STEP - to improve the results:
                    // 0. find all people for the cell and put them in group
                    // 1. calculate average location from all people in the group - this will be the new center of the group
                    // 2. choose appropriate distance (for example radius = half of the cell size, or whole cell size), find all people in this distance and remove the others
                    // 3. if some people were removed in step 2., continue from step 1.

                    // 4. Cleanup - some "different" groups may have moved so much, that they are now close to each other and sharing most of the their people (this depends on chosen size from step 2.) - clean them up

                    // THIRD STEP - make the results human readable (find areas for the location, or city names, or the best way - draw it on the map)

                }
                catch (Exception e)
                {
                    report.IsFinished = true;
                    report.IsError = true;
                    report.ErrorMessage = e.Message;
                    Progress.Report(report);
                }
            });
        }

        private void ReportProgress(ref int unreported, int processed, int totalData, ClusterDataProgressReport report)
        {
            unreported = 0;
            report.Percent = processed * report.MaxPercent / totalData;
            Progress.Report(report);
            CancelToken.ThrowIfCancellationRequested();
        }
    }
}
