﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;

namespace UkPeopleStatsLibrary.ViewModels.Stats
{
    public class PopularWebDomainsViewModel : PopularNamesViewModelBase
    {
        protected override TaskBase<PopularNamesProgressReport> CreateTaskToRun(IProgress<PopularNamesProgressReport> progress, CancellationToken cancellationToken)
        {
            return new CalculatePopularDomainsTask(RecordsToShow, Models.Persistent.Contact.ContactType.Web, SplitWebDomain, progress, cancellationToken);
        }

        private string SplitWebDomain(string wholeWeb)
        {
            try
            {
                var url = new Uri(wholeWeb);

                string[] subs = url.Host.Split(".");

                if (subs.Length >= 2)
                {
                    // get the top level domain
                    return subs[subs.Length - 1];
                }

            }
            catch (Exception)
            {
                // continue to invalid web
            }

            // invalid web
            return null;
        }

        protected override string GetMeasuredPropertyName()
        {
            return "Web Domain";
        }

        protected override string GetTitle()
        {
            return "Web domains";
        }

        protected override string GetShortTitle()
        {
            return "Web";
        }

        protected override string GetDescription()
        {
            return "Most used web domains";
        }
    }
}
