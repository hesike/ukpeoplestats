﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;

namespace UkPeopleStatsLibrary.ViewModels.Stats
{
    public class PopularEmailDomainsViewModel : PopularNamesViewModelBase
    {
        protected override TaskBase<PopularNamesProgressReport> CreateTaskToRun(IProgress<PopularNamesProgressReport> progress, CancellationToken cancellationToken)
        {
            return new CalculatePopularDomainsTask(RecordsToShow, Models.Persistent.Contact.ContactType.Email, SplitEmailDomain, progress, cancellationToken);
        }

        private string SplitEmailDomain(string wholeEmail)
        {
            string[] subs = wholeEmail.Split("@");

            // check if the email is valid (had only one '@')
            if (subs.Length == 2)
            {
                return subs[1];
            }

            // email is invalid, skip it
            return null;
        }

        protected override string GetMeasuredPropertyName()
        {
            return "Email Domain";
        }

        protected override string GetTitle()
        {
            return "Email domains";
        }

        protected override string GetShortTitle()
        {
            return "Email";
        }

        protected override string GetDescription()
        {
            return "Most used email domains";
        }
    }
}
