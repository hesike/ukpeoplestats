﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;

namespace UkPeopleStatsLibrary.ViewModels.Stats
{
    public class PopularFirstNamesViewModel : PopularNamesViewModelBase
    {
        protected override TaskBase<PopularNamesProgressReport> CreateTaskToRun(IProgress<PopularNamesProgressReport> progress, CancellationToken cancellationToken)
        {
            return new CalculatePopularFirstNamesTask(RecordsToShow, progress, cancellationToken);
        }

        protected override string GetMeasuredPropertyName()
        {
            return "First Name";
        }

        protected override string GetTitle()
        {
            return "First names";
        }

        protected override string GetShortTitle()
        {
            return "Names";
        }

        protected override string GetDescription()
        {
            return "Most popular first names";
        }
    }
}
