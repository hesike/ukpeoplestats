﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using UkPeopleStatsLibrary.Models;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;
using UkPeopleStatsLibrary.ViewModels.Commands;

namespace UkPeopleStatsLibrary.ViewModels.Stats
{
    public class PeopleClustersViewModel : NotifyPropertyChangedBase
    {
        private CancellationTokenSource _cancelRefreshSource;

        #region Properties

        private bool _loadCalculationInProgress;
        private bool _clusterCalculationInProgress;
        private bool _calculationInProgress;
        private int _progressPercent;

        public ObservableCollection<LocationCountData> TopClusters { get; } = new ObservableCollection<LocationCountData>();


        public string Title
        {
            get
            {
                return "Most populated locations";
            }
        }

        public string ShortTitle
        {
            get
            {
                return "Locations";
            }
        }

        public string Description
        {
            get
            {
                return "Places with largest groups of people";
            }
        }

        private bool LoadCalculationInProgress
        {
            get { return _loadCalculationInProgress; }
            set
            {
                if (_loadCalculationInProgress == value)
                {
                    // no change (prevent notifications)
                    return;
                }
                _loadCalculationInProgress = value;
                CalculationInProgress = _loadCalculationInProgress | _clusterCalculationInProgress;
            }
        }

        private bool ClusterCalculationInProgress
        {
            get { return _clusterCalculationInProgress; }
            set
            {
                if (_clusterCalculationInProgress == value)
                {
                    // no change (prevent notifications)
                    return;
                }
                _clusterCalculationInProgress = value;
                CalculationInProgress = _loadCalculationInProgress | _clusterCalculationInProgress;
            }
        }

        public bool CalculationInProgress
        {
            get { return _calculationInProgress; }
            private set
            {
                _calculationInProgress = value;
                OnPropertyChanged();
            }
        }

        public int ProgressPercent
        {
            get { return _progressPercent; }
            private set
            {
                _progressPercent = value;
                OnPropertyChanged();
            }
        }

        public bool CanRefresh
        {
            get
            {
                return !CalculationInProgress;
            }
        }
        #endregion

        public PeopleClustersViewModel()
        {
            LoadPostcodesLocationsCommand = new LoadPostcodeLocationsCommand(this, x => CanRefresh, null);
            CalculatePeopleClustersCommand = new CalculatePeopleClustersCommand(this, x => CanRefresh, null);
        }

        #region Commands

        public ICommand LoadPostcodesLocationsCommand { get; }

        public ICommand CalculatePeopleClustersCommand { get; }

        #endregion

        internal async Task LoadPostcodeLocations()
        {
            ProgressPercent = 0;
            LoadCalculationInProgress = true;

            Progress<PercentageProgressReport> progress = new Progress<PercentageProgressReport>();
            _cancelRefreshSource = new CancellationTokenSource();
            progress.ProgressChanged += ReportLoadingProgress;

            await new LoadPostcodeLocationsTask(progress, _cancelRefreshSource.Token).RunAsync();
        }

        internal async Task CalculatePeopleClusters()
        {
            // make sure that all postcode locations are loaded
            await LoadPostcodeLocations();

            ClusterCalculationInProgress = true;

            Progress<ClusterDataProgressReport> progress = new Progress<ClusterDataProgressReport>();
            _cancelRefreshSource = new CancellationTokenSource();
            progress.ProgressChanged += ReportFinalProgress;

            await new CalculatePeopleClustersTask(progress, _cancelRefreshSource.Token).RunAsync();
        }

        private void ReportLoadingProgress(object sender, PercentageProgressReport progress)
        {
            // first part of the progress bar (second is loading calculating clusters) - report up to 50%
            ProgressPercent = progress.Percent / 2;
            if (progress.IsFinished)
            {
                LoadCalculationInProgress = false;
            }
        }

        private void ReportFinalProgress(object sender, ClusterDataProgressReport progress)
        {
            // second part of the progress bar (first is loading location to postcodes) - report up to 100% (but start as 50% since first part of work is done)
            ProgressPercent = (progress.MaxPercent / 2) + progress.Percent / 2;
            if (progress.IsFinished)
            {
                ClusterCalculationInProgress = false;

                if (!progress.IsError)
                {
                    TopClusters.Clear();
                    foreach (LocationCountData cluster in progress.TopClusters)
                    {
                        TopClusters.Add(cluster);
                    }

                }
            }
        }
    }
}
