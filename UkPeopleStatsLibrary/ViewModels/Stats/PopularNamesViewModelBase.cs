﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using UkPeopleStatsLibrary.Models;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;
using UkPeopleStatsLibrary.ViewModels.Commands;

namespace UkPeopleStatsLibrary.ViewModels.Stats
{


    public abstract class PopularNamesViewModelBase : NotifyPropertyChangedBase
    {

        private CancellationTokenSource _cancelRefreshSource;

        #region Properties

        private bool _calculationInProgress;

        public ObservableCollection<NameCount> PopularNames { get; } = new ObservableCollection<NameCount>();

        public bool CalculationInProgress
        {
            get { return _calculationInProgress; }
            private set
            {
                _calculationInProgress = value;
                OnPropertyChanged();
            }
        }

        public bool CanRefresh
        {
            get
            {
                return !CalculationInProgress;
            }
        }

        public string Title
        {
            get
            {
                return GetTitle();
            }
        }

        public string ShortTitle
        {
            get
            {
                return GetShortTitle();
            }
        }

        public string Description
        {
            get
            {
                return GetDescription();
            }
        }

        public string MeasuredPropertyName
        { 
            get
            {
                return GetMeasuredPropertyName();
            }
        }


        public int RecordsToShow { get; private set; } = 10; 
        #endregion

        #region Commands
        public ICommand RefreshDataCommand
        {
            get;
        }
        #endregion

        public PopularNamesViewModelBase()
        {
            RefreshDataCommand = new CalculatePopularNamesDomainsCommand(this, x => CanRefresh, null);
        }

        internal async Task RefreshData()
        {
            CalculationInProgress = true;

            Progress<PopularNamesProgressReport> progress = new Progress<PopularNamesProgressReport>();
            _cancelRefreshSource = new CancellationTokenSource();
            progress.ProgressChanged += ReportProgress;

            await CreateTaskToRun(progress, _cancelRefreshSource.Token).RunAsync();
        }

        protected abstract TaskBase<PopularNamesProgressReport> CreateTaskToRun(IProgress<PopularNamesProgressReport> progress, CancellationToken cancellationToken);

        protected abstract string GetMeasuredPropertyName();
        protected abstract string GetTitle();
        protected abstract string GetShortTitle();

        protected abstract string GetDescription();

        private void ReportProgress(object sender, PopularNamesProgressReport progress)
        {
            if (progress.IsFinished)
            {
                CalculationInProgress = false;

                if (!progress.IsError)
                {
                    PopularNames.Clear();
                    foreach (NameCount popularName in progress.PopularNames)
                    {
                        PopularNames.Add(popularName);
                    }

                }
            }    
        }

    }
}
