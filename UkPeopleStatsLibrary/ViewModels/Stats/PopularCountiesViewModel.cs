﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;

namespace UkPeopleStatsLibrary.ViewModels.Stats
{
    public class PopularCountiesViewModel : PopularNamesViewModelBase
    {
        protected override TaskBase<PopularNamesProgressReport> CreateTaskToRun(IProgress<PopularNamesProgressReport> progress, CancellationToken cancellationToken)
        {
            return new CalculatePopularCountiesTask(RecordsToShow, progress, cancellationToken);
        }

        protected override string GetMeasuredPropertyName()
        {
            return "County";
        }

        protected override string GetTitle()
        {
            return "Counties";
        }

        protected override string GetShortTitle()
        {
            return "County";
        }

        protected override string GetDescription()
        {
            return "Counties with most people";
        }
    }
}
