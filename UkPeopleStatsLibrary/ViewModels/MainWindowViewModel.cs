﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.ViewModels.Commands;
using UkPeopleStatsLibrary.ViewModels.Stats;

namespace UkPeopleStatsLibrary.ViewModels
{
    public class MainWindowViewModel : NotifyPropertyChangedBase
    {
        private AppAllContext _context;

        public Object _popupVM;

        public string Title { get; } = "UkPeopleStats";

        public PeopleViewModel PeopleVM { get; }

        public PeopleClustersViewModel PeopleClustersVM { get; } = new PeopleClustersViewModel();
        public PopularFirstNamesViewModel PopularFirstNamesVM { get; } = new PopularFirstNamesViewModel();
        public PopularCountiesViewModel PopularCountiesVM { get; } = new PopularCountiesViewModel();
        public PopularEmailDomainsViewModel PopularEmailDomainsVM { get; } = new PopularEmailDomainsViewModel();
        public PopularWebDomainsViewModel PopularWebDomainsVM { get; } = new PopularWebDomainsViewModel();

        // TODO RK improve - is this possible to handle in WPF so we won't to duplicate values?
        public bool HasNoPopup
        {
            get { return _popupVM == null; }
        }
        public bool HasPopup
        {
            get { return _popupVM != null; }
        }
        public Object PopupVM
        {
            get { return _popupVM; }
            set
            {
                ICancellableVM cancellableVM = _popupVM as ICancellableVM;

                if (cancellableVM != null)
                {
                    cancellableVM.CancelAction();
                }

                _popupVM = value;

                OnPropertyChanged();
                OnPropertyChanged("HasPopup");
                OnPropertyChanged("HasNoPopup");
            }
        }

        public MainWindowViewModel(AppAllContext context)
        {
            _context = context;

            PeopleVM = new PeopleViewModel(context);

            OpenImport = new RelayCommand(() => !(PopupVM is ImportCsvViewModel), OpenImportPopup);
            OpenAddPerson = new RelayCommand(() => !(PopupVM is PersonViewModel), OpenAddPersonPopup);
            ClosePopup = new RelayCommand(() => true, ClosePopupAction);
        }

        public async Task InitStatData()
        {
            PeopleVM.ReloadData();

            await PopularFirstNamesVM.RefreshData();
            await PopularCountiesVM.RefreshData();
            await PopularEmailDomainsVM .RefreshData();
            await PopularWebDomainsVM.RefreshData();
            await PeopleClustersVM.CalculatePeopleClusters();
        }


        #region Commands

        public RelayCommand OpenImport { get; }

        public RelayCommand OpenAddPerson { get; }

        public RelayCommand ClosePopup { get; }

        #endregion

        private void OpenImportPopup()
        {
            PopupVM = new ImportCsvViewModel(_context);
        }

        private void OpenAddPersonPopup()
        {
            PopupVM = new PersonViewModel(_context);
        }

        private void ClosePopupAction()
        {
            PopupVM = null;
        }

        public void Close()
        {

        }

    }
}
