﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UkPeopleStatsLibrary.ViewModels
{
    public interface ICancellableVM
    {
        public void CancelAction();
    }
}
