﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Tasks;
using UkPeopleStatsLibrary.ViewModels.Commands;

namespace UkPeopleStatsLibrary.ViewModels
{
    public class ImportCsvViewModel : NotifyPropertyChangedBase, ICancellableVM
    {
        AppAllContext _context;

        #region Properties
        private string _filePath;
        private int _totalImported;
        private bool _inProgress;
        private bool _importSuccess;
        private string _errorMessage = "";

        private CancellationTokenSource _cancelImportSource;


        public string FilePath
        {
            get { return _filePath; }
            set
            {
                _filePath = value;
                OnPropertyChanged();
            }
        }

        public int TotalImported
        {
            get { return _totalImported; }
            private set
            {
                _totalImported = value;
                OnPropertyChanged();
            }
        }

        public bool InProgress
        {
            get { return _inProgress; }
            private set
            {
                _inProgress = value;
                OnPropertyChanged();
            }
        }

        public bool ImportSuccess
        {
            get { return _importSuccess; }
            private set
            {
                _importSuccess = value;
                OnPropertyChanged();
            }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            private set
            {
                _errorMessage = value;
                OnPropertyChanged();
            }
        }

        public bool CanImport
        {
            get
            {
                return !InProgress && !String.IsNullOrWhiteSpace(FilePath) && File.Exists(FilePath);
            }
        }

        #endregion

        #region Commands

        // TODO RK fix - button is disabling but not enabling after finishing
        public ICommand ImportDataCommand
        {
            get;
        }

        #endregion

        public ImportCsvViewModel(AppAllContext context)
        {
            _context = context;

            ImportDataCommand = new ImportCsvCommand(this, x => CanImport, null);
        }

        internal async Task ImportData()
        {
            if (CanImport)
            {
                TotalImported = 0;
                ImportSuccess = false;
                ErrorMessage = null;
                InProgress = true;

                Progress<ImportCsvProgressReport> progress = new Progress<ImportCsvProgressReport>();
                _cancelImportSource = new CancellationTokenSource();
                progress.ProgressChanged += ReportProgress;
                await new ImportCsvTask(FilePath, progress, _cancelImportSource.Token).RunAsync();
            }
        }

        internal void ReportProgress(object sender, ImportCsvProgressReport report)
        {
            TotalImported = report.TotalImported;
            if (report.IsFinished)
            {
                if (InProgress)
                {
                    _context.RaiseDataChanged();
                    InProgress = false;
                }
                
                ImportSuccess = !report.IsError;
                ErrorMessage = report.ErrorMessage;
            }
        }

        void ICancellableVM.CancelAction()
        {
            if (_cancelImportSource != null)
            {
                _cancelImportSource.Cancel();
            }
        }
    }
}
