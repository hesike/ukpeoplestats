﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using UkPeopleStatsLibrary.Models.Persistent;
using UkPeopleStatsLibrary.ViewModels.Commands;

namespace UkPeopleStatsLibrary.ViewModels
{
    public class PersonViewModel : NotifyPropertyChangedBase
    {
        private AppAllContext _context;

        #region Properties
        private bool _isSaved = false;
        private Person _dbPerson;
        private Person _person = new Person();

        public Person Person
        {
            get { return _person; }
        }

        public bool IsSaved
        {
            get { return _isSaved; }
            private set
            {
                _isSaved = value;
                OnPropertyChanged();
            }
        }

        public bool CanSave
        {
            get
            {
                return _person.IsValid;
            }
        }
        #endregion

        public PersonViewModel(AppAllContext context) : this(context, new Person { Postcode = new Postcode() })
        {
        }

        public PersonViewModel(AppAllContext context, Person dbPerson)
        {
            _context = context;
            _dbPerson = dbPerson;
            _person.ReplaceDirectData(_dbPerson, true);

            _person.PropertyChanged += (sender, args) => IsSaved = false;

            SaveCommand = new PersonSaveCommand(this);
        }

        public PersonViewModel(PersonViewModel other)
        {
            _context = other._context;
            _dbPerson = other._dbPerson;
            _person.ReplaceDirectData(_dbPerson, true);

            _person.PropertyChanged += (sender, args) => IsSaved = false;

            SaveCommand = new PersonSaveCommand(this);
        }

        #region Commands

        public ICommand SaveCommand
        {
            get;
        }

        #endregion


        public void SaveChanges()
        {
            if (CanSave)
            {
                _dbPerson.ReplaceDirectData(_person, false);

                HandlePostcode();

                // TODO RK improve - handle also contacts

                _context.AppDbContext.Update(_dbPerson);
                _context.AppDbContext.SaveChanges();

                _person.Id = _dbPerson.Id;

                IsSaved = true;
                _context.RaiseDataChanged();
            }
        }

        private void HandlePostcode()
        {
            if (_dbPerson.Postcode.Code != _person.Postcode.Code)
            {
                // postcode did change, try to find if it exists
                Postcode foundPostcode = _context.AppDbContext.Postcodes.Where(p => p.Code == _person.Postcode.Code).SingleOrDefault();

                if (foundPostcode == null)
                {
                    // create the postcode in db
                    foundPostcode = new Postcode();
                    foundPostcode.Code = _person.Postcode.Code;
                    _context.AppDbContext.Add(foundPostcode);
                }

                _dbPerson.Postcode = foundPostcode;
            }

        }

    }
}
