﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using UkPeopleStatsLibrary.Models.Persistent;
using UkPeopleStatsLibrary.ViewModels.Commands;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace UkPeopleStatsLibrary.ViewModels
{
    public class PeopleViewModel : NotifyPropertyChangedBase
    {
        private AppAllContext _context;

        private int _dataLimit = 100;

        #region Properties

        private ObservableCollection<PersonViewModel> _people = new ObservableCollection<PersonViewModel>();

        // filters
        private String _firstNameFilter;
        private String _lastNameFilter;
        private String _postcodeFilter;
        private int _limit;

        public ObservableCollection<PersonViewModel> People
        {
            get { return _people; }
            private set
            {
                _people = value;
                OnPropertyChanged();
            }
        }

        public String FirstNameFilter
        {
            get { return _firstNameFilter; }
            set
            {
                _firstNameFilter = value;
                OnPropertyChanged();
            }
        }

        public String LastNameFilter
        {
            get { return _lastNameFilter; }
            set
            {
                _lastNameFilter = value;
                OnPropertyChanged();
            }
        }

        public String PostcodeFilter
        {
            get { return _postcodeFilter; }
            set
            {
                _postcodeFilter = value;
                OnPropertyChanged();
            }
        }


        public bool CanReloadData
        {
            get
            {
                return true;
            }
        }

        #endregion

        public PeopleViewModel(AppAllContext context)
        {
            _context = context;

            // commands
            ReloadDataCommand = new PeopleReloadCommand(this);

            context.DataChangedEvent += (o, e) => ReloadData();
        }

        #region Commands

        public ICommand ReloadDataCommand
        {
            get;
        }

        #endregion



        public void ReloadData()
        {
            if (CanReloadData)
            {
                // TODO RK improve - make also async
                IQueryable<Person> query = _context.AppDbContext.People.Include(p => p.Postcode);

                if (!String.IsNullOrWhiteSpace(FirstNameFilter))
                {
                    query = query.Where(p => p.FirstName.StartsWith(FirstNameFilter));
                }

                if (!String.IsNullOrWhiteSpace(LastNameFilter))
                {
                    query = query.Where(p => p.LastName.StartsWith(LastNameFilter));
                }

                if (!String.IsNullOrWhiteSpace(PostcodeFilter))
                {
                    query = query.Where(p => p.Postcode.Code.StartsWith(PostcodeFilter));
                }

                // TODO RK improve - let the user decide ordering (maybe even limit)
                var finalQuery = query.OrderBy(p => p.Id).Take(_dataLimit);

                People.Clear();
                foreach (var person in finalQuery)
                {
                    People.Add(new PersonViewModel(_context, person));
                }
            }
        }
    }
}
