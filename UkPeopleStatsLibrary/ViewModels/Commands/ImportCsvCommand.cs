﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using UkPeopleStatsLibrary.DataContext;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    internal class ImportCsvCommand : AsyncCommandBase
    {
        private ImportCsvViewModel _viewModel;

        public ImportCsvCommand(ImportCsvViewModel viewModel, Func<object, bool> canExecute, Action<Exception> onException) : base(canExecute, onException)
        {
            _viewModel = viewModel;
        }

        public async override Task ExecuteAsync(object parameter)
        {
            await _viewModel.ImportData();
        }
    }
}
