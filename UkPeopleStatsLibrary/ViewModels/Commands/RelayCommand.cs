﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    public class RelayCommand : ICommand
    {
        Func<bool> _canExecute;
        Action _execute;

        public RelayCommand(Func<bool> canExecute, Action execute)
        {
            _canExecute = canExecute;
            _execute = execute;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute.Invoke();
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }
    }
}
