﻿
using System;
using System.Windows.Input;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    internal class PersonSaveCommand : ICommand
    {
        private PersonViewModel _viewModel;

        public PersonSaveCommand(PersonViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return _viewModel.CanSave;
        }

        public void Execute(object parameter)
        {
            _viewModel.SaveChanges();
        }
    }
}
