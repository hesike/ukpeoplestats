﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.ViewModels.Stats;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    public class CalculatePeopleClustersCommand : AsyncCommandBase
    {
        private PeopleClustersViewModel _viewModel;

        public CalculatePeopleClustersCommand(PeopleClustersViewModel viewModel, Func<object, bool> canExecute, Action<Exception> onException) : base(canExecute, onException)
        {
            _viewModel = viewModel;
        }

        public async override Task ExecuteAsync(object parameter)
        {
            await _viewModel.CalculatePeopleClusters();
        }
    }
}
