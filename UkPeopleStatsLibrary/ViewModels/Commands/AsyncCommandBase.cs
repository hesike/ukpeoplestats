﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    public abstract class AsyncCommandBase : ICommand
    {
        private Func<object, bool> _canExecute;
        private Action<Exception> _onException;
        private bool _isRunning;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public AsyncCommandBase(Func<object, bool> canExecute, Action<Exception> onException)
        {
            if (canExecute != null)
            {
                _canExecute = canExecute;
            }
            else
            {
                _canExecute = (x => true);
            }
            
            _onException = onException;
        }

        public bool CanExecute(object parameter)
        {
            return !_isRunning && (_canExecute?.Invoke(parameter) ?? true);
        }

        public async void Execute(object parameter)
        {
            _isRunning = true;

            try
            {
                await ExecuteAsync(parameter);
            }
            catch (Exception e)
            {
                _onException?.Invoke(e);
            }
            finally
            {
                _isRunning = false;
            }


        }

        public abstract Task ExecuteAsync(object parameter);

    }
}
