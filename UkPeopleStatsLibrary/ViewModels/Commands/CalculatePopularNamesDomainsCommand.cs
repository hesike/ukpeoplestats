﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.ViewModels.Stats;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    internal class CalculatePopularNamesDomainsCommand : AsyncCommandBase
    {
        private PopularNamesViewModelBase _viewModel;

        public CalculatePopularNamesDomainsCommand(PopularNamesViewModelBase viewModel, Func<object, bool> canExecute, Action<Exception> onException) : base(canExecute, onException)
        {
            _viewModel = viewModel;
        }

        public async override Task ExecuteAsync(object parameter)
        {
            await _viewModel.RefreshData();
        }
    }
}
