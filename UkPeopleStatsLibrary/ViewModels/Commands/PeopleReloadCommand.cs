﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace UkPeopleStatsLibrary.ViewModels.Commands
{
    internal class PeopleReloadCommand : ICommand
    {
        private PeopleViewModel _viewModel;

        public PeopleReloadCommand(PeopleViewModel viewModel)
        {
            _viewModel = viewModel;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public bool CanExecute(object parameter)
        {
            return _viewModel.CanReloadData;
        }

        public void Execute(object parameter)
        {
            _viewModel.ReloadData();
        }
    }
}
