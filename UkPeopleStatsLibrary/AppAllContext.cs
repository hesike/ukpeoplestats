﻿using System;
using System.Collections.Generic;
using System.Text;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Services;

namespace UkPeopleStatsLibrary
{
    public class AppAllContext
    {
        public AppDbContext AppDbContext { get; }
        public PostcodeService PostcodeService { get; }

        public event EventHandler DataChangedEvent;

        public AppAllContext()
        {
            AppDbContext = new AppDbContext();
            PostcodeService = new PostcodeService();
        }

        public void RaiseDataChanged()
        {
            DataChangedEvent?.Invoke(this, new EventArgs());
        }
    }
}
