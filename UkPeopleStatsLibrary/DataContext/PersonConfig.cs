﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UkPeopleStatsLibrary.Models.Persistent;

namespace UkPeopleStatsLibrary.DataContext
{
    public class PersonConfig : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.HasKey(person => person.Id);
            builder.Property(person => person.FirstName).HasMaxLength(200);
            builder.Property(person => person.LastName).HasMaxLength(200);
            builder.Property(person => person.Company).IsRequired(false);
            builder.Property(person => person.Address).IsRequired(false);

            builder.HasOne(person => person.Postcode).WithMany().OnDelete(DeleteBehavior.NoAction);

            builder.HasMany(person => person.Contacts).WithOne().OnDelete(DeleteBehavior.Cascade);


        }
    }
}
