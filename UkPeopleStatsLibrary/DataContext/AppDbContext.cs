﻿using Microsoft.EntityFrameworkCore;
using UkPeopleStatsLibrary.Models.Persistent;

namespace UkPeopleStatsLibrary.DataContext
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        public DbSet<Postcode> Postcodes { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // TODO RK improve - put this somewhere else
            optionsBuilder.UseSqlite("Data Source=data.db");

            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new PersonConfig());
            modelBuilder.ApplyConfiguration(new PostcodeConfig());

        }
    }
}
