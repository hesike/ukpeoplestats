﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using UkPeopleStatsLibrary.Models.Persistent;

namespace UkPeopleStatsLibrary.DataContext
{
    public class PostcodeConfig : IEntityTypeConfiguration<Postcode>
    {
        public void Configure(EntityTypeBuilder<Postcode> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Code).HasMaxLength(20).IsRequired(true);
            builder.Property(c => c.Latitude).IsRequired(false);
            builder.Property(c => c.Longitude).IsRequired(false);

            builder.HasIndex(c => c.Code).IsUnique();
        }
    }
}
