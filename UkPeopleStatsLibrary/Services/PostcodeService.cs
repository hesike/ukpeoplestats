﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using UkPeopleStatsLibrary.Models.Persistent;

namespace UkPeopleStatsLibrary.Services
{
    public class PostcodeService
    {
        // would be better to have it somewhere else and inject it
        private static readonly HttpClient client = new HttpClient();

        private const string baseUrl = "https://api.postcodes.io/postcodes/";

        class PostcodeResult
        {
            [JsonPropertyName("latitude")]
            public double? Latitude { get; set; }

            [JsonPropertyName("longitude")]
            public double? Longitude { get; set; }
        }

        class PostcodeApiReply
        {
            [JsonPropertyName("status")]
            public int Status { get; set; }

            [JsonPropertyName("result")]
            public PostcodeResult Result { get; set; }

            [JsonPropertyName("error")]
            public string Error { get; set; }
        }

        public async Task FillLocationAsync(Postcode postcode)
        {
            // throws exception in case of connection problems

            

            try
            {
                HttpRequestMessage request = new HttpRequestMessage();
                request.RequestUri = new Uri(baseUrl + postcode.Code);
                request.Method = HttpMethod.Get;
                HttpResponseMessage response = await client.SendAsync(request);

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    string replyData = await response.Content.ReadAsStringAsync();
                    PostcodeApiReply reply = JsonSerializer.Deserialize<PostcodeApiReply>(replyData);
                    postcode.Latitude = reply.Result.Latitude;
                    postcode.Longitude = reply.Result.Longitude;
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    // mark postcode as invalid so we know it's processed and won't try to resolve it next time
                    postcode.MarkAsInvalid();
                }

            }
            catch (Exception)
            {
                // there was a connection error, do nothing (try next time)
            }

        }
    }
}
