﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UkPeopleStatsLibrary.ViewModels;

namespace UkPeopleStatsUi.Views
{
    /// <summary>
    /// Interaction logic for PeopleTableView.xaml
    /// </summary>
    public partial class PeopleTableView : UserControl
    {
        public PeopleTableView()
        {
            InitializeComponent();
        }

        private void DataGridRow_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DataGridRow target = (DataGridRow)sender;

            MainWindowViewModel mainViewModel = (MainWindowViewModel) DataContext;

            PersonViewModel rowVm = target.DataContext as PersonViewModel;

            if (rowVm != null)
            {
                mainViewModel.PopupVM = new PersonViewModel(rowVm);
            }
            
        }
    }
}
