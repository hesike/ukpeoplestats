﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UkPeopleStatsLibrary;
using UkPeopleStatsLibrary.ViewModels;

namespace UkPeopleStatsUi.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel mainWindowViewModel;

        public MainWindow()
        {
            AppAllContext context = new AppAllContext();

            context.AppDbContext.Database.EnsureCreated();

            mainWindowViewModel = new MainWindowViewModel(context);

            DataContext = mainWindowViewModel;

            InitializeComponent();
        }

        private async void Window_Initialized(object sender, EventArgs e)
        {
            await mainWindowViewModel.InitStatData();
        }
    }
}
