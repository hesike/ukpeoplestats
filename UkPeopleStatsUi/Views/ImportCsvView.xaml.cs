﻿using Microsoft.Win32;
using System.Windows;
using System.Windows.Controls;
using UkPeopleStatsLibrary.ViewModels;

namespace UkPeopleStatsUi.Views
{
    /// <summary>
    /// Interaction logic for ImportCsvView.xaml
    /// </summary>
    public partial class ImportCsvView : UserControl
    {
        public ImportCsvView()
        {
            InitializeComponent();
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            ImportCsvViewModel viewModel = this.DataContext as ImportCsvViewModel;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                viewModel.FilePath = openFileDialog.FileName;
        }
    }
}
