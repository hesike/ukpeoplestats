using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;
using UkPeopleStatsLibrary;
using UkPeopleStatsLibrary.DataContext;
using UkPeopleStatsLibrary.Models.Persistent;
using UkPeopleStatsLibrary.Models.ProgressReports;
using UkPeopleStatsLibrary.Services;
using UkPeopleStatsLibrary.Tasks;
using UkPeopleStatsLibrary.ViewModels;
using UkPeopleStatsLibrary.ViewModels.Stats;
using Xunit;

namespace UkPeopleStatsUnitTests
{
    public class BasicTests
    {


        [Fact]
        public async Task TestLoadingPostcodeLocationsFromApi()
        {
            Postcode postcode = new Postcode();
            postcode.Code = "GU50BD"; // this should be valid postcode

            await new PostcodeService().FillLocationAsync(postcode);

            Assert.True(postcode.IsProcessed);
            Assert.True(!postcode.IsInvalid);
            Assert.NotNull(postcode.Latitude);
            Assert.NotNull(postcode.Longitude);
        }

        [Fact]
        public async Task InvalidPostcodeIsMarkedSo()
        {
            Postcode postcode = new Postcode();
            postcode.Code = "invalid";

            await new PostcodeService().FillLocationAsync(postcode);

            Assert.True(postcode.IsProcessed);
            Assert.True(postcode.IsInvalid);
        }

        [Fact]
        public void InvalidPersonCantBeSaved()
        {
            PersonViewModel personViewModel = new PersonViewModel(new AppAllContext());

            // data are not filled, can't be saved
            Assert.True(!personViewModel.CanSave);

            Person person = personViewModel.Person;

            person.FirstName = "test";
            person.LastName = "test";
            person.Company = "test";
            person.Address = "test";
            person.City = "test";
            person.County = "test";
            person.Postcode.Code = "test";

            // can be saved now
            Assert.True(personViewModel.CanSave);

            person.FirstName = "";

            // there is an error for first name
            Assert.True(((IDataErrorInfo)person)["FirstName"] != null);

            Assert.True(!personViewModel.CanSave);
        }
    }

    [Collection("Sequential")]
    public class TestUsingDb
    {
        [Fact]
        public void PeopleAreLoadedWithPostcodes()
        {
            const string POSTCODE = "POSTCODE";

            {
                // init db
                AppDbContext dbContext = new AppDbContext();
                dbContext.Database.EnsureDeleted();
                dbContext.Database.EnsureCreated();

                Postcode postcode = new Postcode();
                postcode.Code = POSTCODE;

                dbContext.Add(postcode);

                Person person = new Person();
                person.FirstName = "test";
                person.LastName = "test";
                person.Company = "test";
                person.Address = "test";
                person.City = "test";
                person.County = "test";
                person.Postcode = postcode;

                dbContext.Add(person);
                dbContext.SaveChanges();
            }

            AppAllContext appContext = new AppAllContext();
            PeopleViewModel peopleViewModel = new PeopleViewModel(appContext);

            peopleViewModel.ReloadData();

            Assert.True(peopleViewModel.People.Count == 1);
            Assert.True(peopleViewModel.People[0].Person.Postcode.Code == POSTCODE);
        }

        [Fact]
        public async Task CalculatingPopularDomainsWork()
        {
            {
                // init db
                AppDbContext dbContext = new AppDbContext();
                dbContext.Database.EnsureDeleted();
                dbContext.Database.EnsureCreated();

                Person person = new Person();
                person.FirstName = "test";
                person.LastName = "test";
                person.Company = "test";
                person.Address = "test";
                person.City = "test";
                person.County = "test";
                person.Postcode = new Postcode { Code = "Test" };

                person.Contacts.Add(new Contact { Type = Contact.ContactType.Email, Value = "aaa@something.com" });
                person.Contacts.Add(new Contact { Type = Contact.ContactType.Email, Value = "bbb@domain.com" });
                person.Contacts.Add(new Contact { Type = Contact.ContactType.Email, Value = "ccc@domain.com" });
                person.Contacts.Add(new Contact { Type = Contact.ContactType.Email, Value = "ddd@else.com" });
                person.Contacts.Add(new Contact { Type = Contact.ContactType.Email, Value = "eee@domain.com" });
                person.Contacts.Add(new Contact { Type = Contact.ContactType.Email, Value = "fff@else.com" });

                dbContext.People.Add(person);
                dbContext.SaveChanges();
            }

            PopularEmailDomainsViewModel viewModel = new PopularEmailDomainsViewModel();

            viewModel.RefreshDataCommand.Execute(null);


            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(100);
                if (!viewModel.CalculationInProgress)
                {
                    // calculation finished, check values
                    Assert.True(viewModel.PopularNames.Count == 3);
                    Assert.True(viewModel.PopularNames[0].Name == "domain.com" && viewModel.PopularNames[0].Count == 3);
                    Assert.True(viewModel.PopularNames[1].Name == "else.com" && viewModel.PopularNames[1].Count == 2);
                    Assert.True(viewModel.PopularNames[2].Name == "something.com" && viewModel.PopularNames[2].Count == 1);

                    // all done
                    return;
                }
            }

            // the test failed, values were never ready
            Assert.True(false);
        }
    }

}
